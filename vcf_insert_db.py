import re
import sys
import pymysql
from datetime import datetime, timedelta
import gzip
import os
import json
import pymysql.cursors
import hashlib

greece_time =  '{:%Y-%m-%d--%H:%M:%S}'.format(datetime.now() + timedelta(hours=3))
gnomad_ids = []
total_file_headers = list()
def getHeaders(filename, sequencing):
    total_list = list()
    def toString(s):
        try:
            return s.decode('utf-8')
        except (UnicodeDecodeError, AttributeError):
            return s

    gzipped = filename.endswith('.gz')
    with (gzip.open(filename) if gzipped else open(filename)) as fp:
        line_number = 0
        line = toString(fp.readline())
        line_number += 1

        format_headers_list = list()
        info_headers_list = list()
        vep_headers_list = dict()
        while line and line.startswith('#'):
            if line not in total_file_headers:
                total_file_headers.append(line)
            if "##VEP" in line and line.startswith('##'):
                ## VEP VERSION INFO, INSERTED INTO SCREENINGS
                line = re.sub('##', '', line)
                line_splitted = re.sub(r'(?!(([^"]*"){2})*[^"]*$) ', '---', line);
                vep_splitted = line_splitted.split(" ")
                for key,i in enumerate(vep_splitted):
                    vep_splitted[key] = i.replace("---",",")
                    try:
                        dict_key, dict_val = vep_splitted[key].split("=",1)
                    except:
                        continue
                    else:
                        vep_headers_list.update({dict_key:dict_val})
                    
            if "FORMAT" in line and line.startswith('##'):
                ## FORMAT COLUMNS DESCRIPTION INSERTED INTO TABLE COLUMNS
                line = re.sub('[<>\n]', '', line)
                format = line.split("=",1)[1]
                format_replaced = re.sub(r'(?!(([^"]*"){2})*[^"]*$),', '---', format);
                format_splitted = format_replaced.split(",")
                test_dict_format = dict()
                for key,i in enumerate(format_splitted):
                    format_splitted[key] = i.replace("---",",")
                    try:
                        dict_key, dict_val = format_splitted[key].split("=",1)
                    except:
                        continue
                    else:
                        if 'ID' in dict_key:
                            if sequencing == 'Illumina':
                                dict_val = "ILLUMINA_FORMAT_"+dict_val
                            else:
                                dict_val = "ION_FORMAT_"+dict_val
                        test_dict_format.update({dict_key:dict_val})

                format_headers_list.append((test_dict_format))
            if "INFO" in line and "CSQ" not in line and line.startswith('##'):
                ## INFO COLUMNS DESCRIPTION INSERTED INTO TABLE COLUMNS
                line = re.sub('[<>\n]', '', line)
                info = line.split("=",1)[1]
                info_replaced = re.sub(r'(?!(([^"]*"){2})*[^"]*$),', '---', info);
                info_splitted = info_replaced.split(",")
                test_dict_info = dict()
                for key,i in enumerate(info_splitted):
                    info_splitted[key] = i.replace("---",",")
                    try:
                        dict_key, dict_val = info_splitted[key].split("=",1)
                    except:
                        continue
                    else:
                        if 'ID' in dict_key:
                            if sequencing == 'Illumina':
                                dict_val = "ILLUMINA_INFO_"+dict_val
                            else:
                                dict_val = "ION_INFO_"+dict_val
                        test_dict_info.update({dict_key:dict_val})
                info_headers_list.append((test_dict_info))

            
            line = toString(fp.readline())
            line_number += 1
        total_list.append(info_headers_list)
        total_list.append(format_headers_list)
        total_list.append(vep_headers_list)
        return [total_list,total_file_headers]


def getVariantDescription(variant, ref, alt):
    hgvs_prefix = 'm.' if variant['chromosome'] == 'M' else 'g.'
    alt_original = alt
    while len(ref) > 0 and len(alt) > 0 and ref[0] == alt[0]:

        ref = ref[1:]
        alt = alt[1:]
        variant['position_g_start'] += 1

    while len(ref) > 0 and len(alt) > 0 and ref[-1] == alt[-1]:
        ref = ref[:-1]
        alt = alt[:-1]
        variant['position_g_end'] -= 1

    if len(ref) > 0 and len(alt) == 0:
        variant['type'] = 'del'
        if variant['position_g_start'] == variant['position_g_end']:
            variant['VariantOnGenome/DNA'] = hgvs_prefix + \
                str(variant['position_g_start'])+'del'
        else:
            variant['VariantOnGenome/DNA'] = hgvs_prefix + \
                str(variant['position_g_start'])+'_' + \
                str(variant['position_g_end'])+'del'
    elif len(alt) > 0 and len(ref) == 0:
        if alt_original[alt_original.rfind(alt)-len(alt):alt_original.rfind(alt)] == alt:
            variant['type'] = 'dup'
            variant['position_g_start'] -= len(alt)
            if variant['position_g_start'] == variant['position_g_end']:
                variant['VariantOnGenome/DNA'] = hgvs_prefix + \
                    str(variant['position_g_start'])+'dup'
            else:
                variant['VariantOnGenome/DNA'] = hgvs_prefix + \
                    str(variant['position_g_start'])+'_' + \
                    str(variant['position_g_end'])+'dup'
        else:
            variant['type'] = 'ins'
            variant['position_g_start'] -= 1
            variant['position_g_end'] += 1
            variant['VariantOnGenome/DNA'] = hgvs_prefix + \
                str(variant['position_g_start'])+'_' + \
                str(variant['position_g_end'])+'ins'+alt
    elif len(alt) == 1 and len(ref) == 1:
        variant['type'] = 'subst'
        variant['VariantOnGenome/DNA'] = hgvs_prefix + \
            str(variant['position_g_start'])+ref+'>'+alt
    elif ref == alt.lower().replace('a', 'T').replace('c', 'G').replace('g', 'C').replace('t', 'A')[::-1]:
        variant['type'] = 'inv'
        variant['VariantOnGenome/DNA'] = hgvs_prefix + \
            str(variant['position_g_start'])+'_' + \
            str(variant['position_g_end'])+'inv'
    else:
        variant['type'] = 'delins'
        if variant['position_g_start'] == variant['position_g_end']:
            variant['VariantOnGenome/DNA'] = hgvs_prefix + \
                str(variant['position_g_start'])+'delins'+alt
        else:
            variant['VariantOnGenome/DNA'] = hgvs_prefix + \
                str(variant['position_g_start'])+'_' + \
                str(variant['position_g_end'])+'delins'+alt


def readVariants(filename):
    def toString(s):
        try:
            return s.decode('utf-8')
        except (UnicodeDecodeError, AttributeError):
            return s

    gzipped = filename.endswith('.gz')
    with (gzip.open(filename) if gzipped else open(filename)) as fp:
        line_number = 0
        line = toString(fp.readline())
        line_number += 1
        while line and line.startswith('##'): #line.startswith('#')
            line = toString(fp.readline())
            line_number += 1
            
            
        headers = line.strip('# \t\n').split('\t')

        for line in fp:
            line = toString(line)
            line_number += 1
            vcf_line = dict(zip(headers, line.strip().split('\t')))
            
            # SEARCH FOR CHROMOSOME FIELD 
            
            m = re.search(
                '^(?:c(?:hr)?)?([XYM]|[1-9]|1[0-9]|2[0-2])$', vcf_line['CHROM'])
            if not m:
                print(
                    f"Error parsing chromosome on line {line_number}.", file=sys.stderr)
                continue
            chromosome = m.group(1)
            
            # SEARCH FOR RS IDENTIFIER FIELD 
            
            m = re.search('(?:^|;)(rs\d+)(?:$|;)', vcf_line['ID'])
            reference = m.group(1) if m else None

            used_alleles = set()
            format = vcf_line['FORMAT'].split(':')
            info = vcf_line['INFO'].split(';')

            info_dict = dict()
            for i in info:

                key_val = i.split("=")
                try:
                    info_dict.update({key_val[0]:key_val[1]})
                except:
                    info_dict.update({key_val[0]:' '})

            for i in range(9, len(headers)):
                vcf_line[headers[i]] = dict(
                    zip(format, vcf_line[headers[i]].split(':')))
           
            vcf_line['REF'] = vcf_line['REF'].upper()
            alleles = vcf_line['ALT'].upper().split(',')
            test = 0
            for allele_number, allele in enumerate(alleles):

                
                variant = {'chromosome': chromosome}
                variant['gnomad_id'] = "%s-%s-%s-%s" % (
                    chromosome, vcf_line['POS'], vcf_line['REF'], allele)
                
                try:
                    variant['qual'] = float(vcf_line['QUAL'])
                except:
                    pass
                if reference:
                    variant['VariantOnGenome/Reference'] = '{dbSNP:%s}' % reference
                else:
                    variant['VariantOnGenome/Reference'] = None

                try:
                    variant['position_g_start'] = int(vcf_line['POS'])
                except:
                    variant['position_g_start'] = 0
                    print(
                        f"Error parsing POS field on line {line_number}.", file=sys.stderr)
                variant['position_g_end'] = variant['position_g_start'] + \
                    len(vcf_line['REF'])-1

                variant['allele'] = 0
                if(len(headers) == 10):  # If we have one sample
                    if vcf_line[headers[9]]['GT'][0] == allele_number+1:
                        variant['allele'] += 1
                    if vcf_line[headers[9]]['GT'][1] == allele_number+1:
                        variant['allele'] += 2

                getVariantDescription(variant, vcf_line['REF'], allele)
                variant['allele'] = allele_number+1
                variant['REF'] = vcf_line['REF']
                variant['ALT'] = allele
                if 'AF' in vcf_line[headers[9]]: # DEBUG FOR EIE HERED. VCFS
                    if len(vcf_line[headers[9]]['AF'].split(",")) == len(alleles):
                        variant['AF'] = vcf_line[headers[9]]['AF'].split(",")[allele_number] # APPEND RESPECTIVE AF AT EACH VARIANT
                    else:
                        variant['AF'] = vcf_line[headers[9]]['AF']
                else:  # DEBUG FOR EIE HERED. VCFS
                    variant['AF'] = None  # DEBUG FOR EIE HERED. VCFS

                if 'FDP' in info_dict:
                    if len(info_dict['FDP'].split(",")) == len(alleles):
                        variant['FDP'] = info_dict['FDP'].split(",")[allele_number]
                    else:
                        variant['FDP'] = info_dict['FDP']
                else:
                    variant['FDP'] = None
                    
                if 'TDP' in info_dict:
                    if len(info_dict['TDP'].split(",")) == len(alleles):
                        variant['TDP'] = info_dict['TDP'].split(",")[allele_number]
                    else:
                        variant['TDP'] = info_dict['TDP']
                else:
                    variant['TDP'] = None
                    
                if 'MBQ' in info_dict:
                    if len(info_dict['MBQ'].split(",")) == len(alleles):
                        variant['MBQ'] = info_dict['MBQ'].split(",")[allele_number]
                    else:
                        variant['MBQ'] = info_dict['MBQ']
                else:
                    variant['MBQ'] = None

                if any (k in vcf_line[headers[9]].keys() for k in ("FSRF","FSAF","FSRR","FSAR")):
                    
                  
                    if len(vcf_line[headers[9]]['FSRF'].split(",")) == len(alleles):
                        FSRF = float(vcf_line[headers[9]]['FSRF'].split(",")[allele_number])
                    else:
                        FSRF = float(vcf_line[headers[9]]['FSRF'])

                    if len(vcf_line[headers[9]]['FSAF'].split(",")) == len(alleles):
                        FSAF = float(vcf_line[headers[9]]['FSAF'].split(",")[allele_number])
                    else:
                        FSAF = float(vcf_line[headers[9]]['FSAF'])

                    if len(vcf_line[headers[9]]['FSRR'].split(",")) == len(alleles):
                        FSRR = float(vcf_line[headers[9]]['FSRR'].split(",")[allele_number])
                    else:
                        FSRR = float(vcf_line[headers[9]]['FSRR'])

                    if len(vcf_line[headers[9]]['FSAR'].split(",")) == len(alleles):
                        FSAR = float(vcf_line[headers[9]]['FSAR'].split(",")[allele_number])
                    else:
                        FSAR = float(vcf_line[headers[9]]['FSAR'])
                    
                    try:
                        variant['STB'] = (FSRF+FSAF)/(FSRF+FSAF+FSRR+FSAR)
                    except:
                        variant['STB'] = None

                    try:
                        variant['AltSTB'] = FSAF/(FSAF+FSAR)
                    except:
                        variant['AltSTB'] = None 

                    try:
                        variant['RefSTB'] = FSRF/(FSRF+FSRR)
                    except:
                        variant['RefSTB'] = None
                else:
                    variant['STB'] = None
                    variant['AltSTB'] = None
                    variant['RefSTB'] = None

                if 'SAPP' in info_dict:
                    variant['ForwardSTA'] = float(info_dict['SAPP'].split(",")[0])
                    variant['ReverseSTA'] = float(info_dict['SAPP'].split(",")[1])
                    variant['NoSTA'] = float(info_dict['SAPP'].split(",")[2])
                else:
                    variant['ForwardSTA'] = None
                    variant['ReverseSTA'] = None
                    variant['NoSTA'] = None
                    

                format_dict_copy = vcf_line[headers[9]].copy()
                info_dict_copy = info_dict.copy()

                variant['FORMAT'] = format_dict_copy
                variant['INFO'] = info_dict_copy
                variant['REF'] = vcf_line['REF']
                variant['ALT'] = vcf_line['ALT']
                variant['POS'] = vcf_line['POS']
                variant['Variant_ID'] = vcf_line['ID']
                if not vcf_line['QUAL'] :
                    vcf_line['QUAL'] = ''
                if not vcf_line['FILTER'] :
                    vcf_line['FILTER'] = ''
                variant['QUAL'] = vcf_line['QUAL']
                variant['FILTER'] = vcf_line['FILTER']
                yield variant

def insert(mysql_host, mysql_username, mysql_password, mysql_database, filename, orderId,s3Location):
   
    MAPPING_ALLOW = 1
    STATUSID_PUBLIC = 9
    EFFECTID_UNKNOWN = 55
    OWNER_LOVD = 0

    files = list()
    sequencing = list()
    ref_num = list()
    s3_locations = list()
    s3_locations.append(s3Location)




    files.append(filename)
    ref_num.append(orderId)


    total_variants = list()
    format_headers = list()
    info_headers = list()
    total_variants = list()
    variants = [None] * len(files)
    sequencing = [None] * len(files)
    hashes = [None] * len(files)
    vcf_headers = [None] * len(files)
    commit = [1] * len(files)
    file_headers = [None] * len(files)


    log_filename = 'vcf_insert_db.txt'

    if(os.path.exists(log_filename)):
        f = open(log_filename,'a+')
    else:
        f = open(log_filename, 'w')


    for ind,file in enumerate(files):

        print("Filtering variants for file %s." % ( file ), file=sys.stdout) 
        f.write("Filtering variants for file %s.\n" % ( file ) )

        gzipped = file.endswith('.gz')
        with (gzip.open(file) if gzipped else open(file)) as fp:
            if gzipped:
                line = fp.read().decode('utf-8')
            else:
                line = fp.read()
            if 'INFO=<ID=FDP' in line:
                sequencing[ind] = 'Ion torrent'
            else:
                sequencing[ind] = 'Illumina'

        # GENERATE VCF FILE HASH
        hashes[ind] = hashlib.md5(open(file,'rb').read()).hexdigest()
        variants[ind] = [variant for variant in readVariants(file)]

        print("Getting headers for file %s." % ( file ), file=sys.stdout )
        f.write("Getting headers for file %s.\n" % ( file ) )

        # GET VCF FILE HEADERS
        # vcf_headers[ind] = getHeaders(file, sequencing[ind])[0]
        # total_file_headers[ind] = getHeaders(file, sequencing[ind])[1]
        # vep = vcf_headers[ind][2]

        headers = getHeaders(file, sequencing[ind])
        vcf_headers[ind] = headers[0]
        file_headers[ind] = headers[1]
        vep = vcf_headers[ind][2]
        
        for i in vcf_headers[ind][0]:
            if 'Description' in i:
                info_headers.append((i['ID'], i['Description']))
        for i in vcf_headers[ind][1]:
            if 'Description' in i:
                format_headers.append((i['ID'], i['Description']))     

    headers_string = "".join([str(elem) for elem in file_headers[0]])

    print("Connecting to mysql %s database." % ( mysql_database ), file=sys.stdout)
    f.write("Connecting to mysql %s database.\n" % ( mysql_database ) )

    try:
        connection = pymysql.connect(host=mysql_host,
                                    user=mysql_username,
                                    password=mysql_password,
                                    database=mysql_database,
                                    cursorclass=pymysql.cursors.DictCursor,
                                    autocommit=False)
    except pymysql.err.OperationalError as e:
        print("Could not connect to mysql %s database." % ( mysql_database ) + " Error: " + str(e) + "\n", file=sys.stdout )
        f.write(str(greece_time) + ": Could not connect to mysql %s database." % ( mysql_database ) + " Error: " + str(e) + "\n" )
        
    connection.autocommit = False 

    insert_info_columns_query = 'INSERT IGNORE INTO lovd_columns (id, description_form) values (%s, %s)'

    individual_exists_query = "SELECT lovd_individuals.id FROM lovd_individuals WHERE lovd_individuals.`Individual/Reference` = '%s' AND hash = '%s'"

    insert_individuals_query = '''INSERT INTO lovd_individuals (`Individual/Reference`, `created_date`, `hash`, `s3_location`) values (%s, %s, %s, %s)'''

    insert_screenings_query = '''INSERT INTO lovd_screenings (`individualid`, `Sequencing/Machine`, `created_date`, `Filename`, `file_headers`, `hash`) values (%s, %s, %s, %s, %s, %s)'''

    insert_variants_query = '''insert into lovd_variants (allele, Variant_ID, qual, filter, chromosome, ref, alt, mbq, pos, position_g_start, position_g_end, type, 
            `VariantOnGenome/DNA`, `VariantOnGenome/Reference`, AF, TDP, FDP, STB, ALTSTB, REFSTB,
            FORWARDSTA, REVERSESTA, NOSTA, created_date, mapping_flags, statusid, effectid, owned_by, INFO_FIELDS, FORMAT_FIELDS, hash) values 
            (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'''

    insert_variants_screenings_query =   '''INSERT INTO lovd_screenings2variants (`variantid`, `screeningid`) 
                                            SELECT lovd_variants.id AS variantsid, lovd_screenings.id AS screeningid 
                                            FROM lovd_variants, lovd_screenings 
                                            JOIN lovd_individuals 
                                            ON lovd_individuals.id = lovd_screenings.individualid
                                            WHERE lovd_variants.hash = "%s" 
                                            AND lovd_screenings.id = "%s"
                                            AND lovd_individuals.`Individual/Reference` = "%s"
                                            AND lovd_variants.id NOT IN (SELECT variantid FROM lovd_screenings2variants)'''

    with connection.cursor() as cursor:
        
        for file_num in range(len(variants)):

            try:
                cursor.executemany(insert_info_columns_query, info_headers)
            except (pymysql.Error, pymysql.Warning) as e:
                print("Non breaking error: Info headers insertion failed. Exception: " + str(e), file=sys.stdout)
                f.write("Non breaking error: Info headers insertion failed. Exception: %s \nQuery Executed: \n%s\n" % ( str(e), str(cursor._last_executed) ) )
            
            try:
                cursor.executemany(insert_info_columns_query, format_headers)
            except (pymysql.Error, pymysql.Warning) as e:
                print("Non breaking error: Format headers insertion failed. Exception: " + str(e), file=sys.stdout)
                f.write("Non breaking error: Format headers insertion failed. Exception: %s \nQuery Executed: \n%s\n" % ( str(e), str(cursor._last_executed) ) )

            commit[file_num] = 1
            screening_id = -1
            individual_id = -1
            filename_to_insert = files[file_num].split("/")[-1]
            filename_to_insert = re.sub(ref_num[file_num]+"__","",filename_to_insert)
            timestamp_l = re.findall(r"\d+-\d+-\d+--\d+:\d+:\d+__", filename_to_insert)
            filename_to_insert = re.sub(r"\d+-\d+-\d+--\d+:\d+:\d+__","",filename_to_insert)
            try:
                timestamp = timestamp_l[0].replace("_","").replace("--"," ")
            except:
                timestamp = greece_time
            
            # CHECK IF VARIANT FILE EXISTS IN DB IN COMB WITH THE REFERRAL NUMBER 
            try:
                cursor.execute( individual_exists_query % ( ref_num[file_num], hashes[file_num]) )
            except (pymysql.Error, pymysql.Warning) as e:
                print("Non breaking error: " + str(e), file=sys.stdout)
                f.write(str(greece_time) + "Non breaking error: " + str(e) + "\n" + "Query executed: \n" + str(cursor._last_executed) )

            results = cursor.fetchone()

            if results is not None and results['id'] is not None:
                print("Aborting Error: Individual with reference number %s and hash %s found in database. Aborting." % ( ref_num[file_num], hashes[file_num] ), file=sys.stdout )
                f.write("Aborting Error: Individual with reference number %s and hash %s found in database. Aborting.\n" % ( ref_num[file_num], hashes[file_num] ) + "\n" + "Query executed: \n" + str(cursor._last_executed) )
                individual_id = results['id']
                print("individual_id:", individual_id, file=sys.stdout )
                print("hash:", hashes[file_num], file=sys.stdout )
                f.write("individual_id: %s \n" % str(individual_id) )
                f.write("hash: %s \n" % str(hashes[file_num]) )
                commit[file_num] = 0
                continue
            else:

                sql = '''INSERT INTO lovd_individuals (`Individual/Reference`, `created_date`, `hash`, `s3_location`) values (%s, %s, %s, %s)'''
                try:
                    cursor.execute(sql,(ref_num[file_num], timestamp, hashes[file_num], s3_locations[file_num]))
                except (pymysql.Error, pymysql.Warning) as e:
                    print("Breaking Error: Individuals query exception: " + str(e), file=sys.stdout )
                    f.write(str(greece_time) + "Breaking Error: Individuals query exception: " + str(e) + "\n" + "Query executed: \n" + str(cursor._last_executed) )
                individual_id = cursor.lastrowid

                if ( cursor.rowcount != 0 ):
                    print("individual_id:", individual_id, file=sys.stdout )
                    print("hash:", hashes[file_num] )
                    f.write("individual_id: %s \n" % str(individual_id) )
                    f.write("hash: %s \n" % str(hashes[file_num]) )
                else:
                    print("individual_id:", individual_id, file=sys.stdout )  
                    f.write("individual_id: %s \n" % str(individual_id) )
                    commit[file_num] = 0 
                    continue        

            try:
                cursor.execute(insert_screenings_query,(individual_id, sequencing[file_num], timestamp, filename_to_insert, json.dumps(headers_string), hashes[file_num]))
            except (pymysql.Error, pymysql.Warning) as e:
                print("Breaking Error: Individuals query exception: " + str(e), file=sys.stdout )
                f.write(str(greece_time) + "Breaking Error: Individuals query exception: " + str(e) + "\n" + "Query executed: \n" + str(cursor._last_executed) )

            screening_id = cursor.lastrowid
            if ( cursor.rowcount != 0 ):
                print("screening_id:", screening_id, file=sys.stdout )
                f.write("screening_id: %s \n" % str(screening_id) )
            else:
                print("screening_id:", screening_id, file=sys.stdout )
                f.write("screening_id: %s \n" % str(screening_id) )
                commit[file_num] = 0
                continue

            variants2screenings = list()
            variants_data = list()
            for row, var in enumerate(variants[file_num]):
                var = {k: None if v == '' else v for k, v in var.items() }

                variants_data.append((var['allele'], 
                                    var['Variant_ID'], 
                                    var['QUAL'], 
                                    var['FILTER'], 
                                    var['chromosome'], 
                                    var['REF'], 
                                    var['ALT'], 
                                    var['MBQ'], 
                                    var['POS'], 
                                    var['position_g_start'], 
                                    var['position_g_end'], 
                                    var['type'],
                                    var['VariantOnGenome/DNA'], 
                                    var['VariantOnGenome/Reference'], 
                                    var['AF'], 
                                    var['TDP'],
                                    var['FDP'], 
                                    var['STB'],
                                    var['AltSTB'], 
                                    var['RefSTB'], 
                                    var['ForwardSTA'],
                                    var['ReverseSTA'],
                                    var['NoSTA'],
                                    timestamp, 
                                    MAPPING_ALLOW, 
                                    STATUSID_PUBLIC, 
                                    EFFECTID_UNKNOWN, 
                                    OWNER_LOVD,
                                    json.dumps(var['INFO']),
                                    json.dumps(var['FORMAT']),
                                    hashes[file_num]))


            try:
                cursor.executemany(insert_variants_query, variants_data)
            except (pymysql.Error, pymysql.Warning) as e:
                print("Breaking Error: Variants query exception: " + str(e), file=sys.stdout )
                f.write(str(greece_time) + "Breaking Error: Variants query exception: " + str(e) + "\n" + "Query executed: \n" + str(cursor._last_executed) )
                commit[file_num] = 0
                continue

            if ( cursor.rowcount != 0 ):
                print("variants_size:", cursor.rowcount, file=sys.stdout )
                f.write("variants_size: %d \n" % int(cursor.rowcount) )
            else:
                print("variants_size:", 0, file=sys.stdout )
                f.write("variants_size: 0 \n")
                commit[file_num] = 0
                continue

            try:
                cursor.execute(insert_variants_screenings_query % ( hashes[file_num], screening_id, ref_num[file_num] ) )
            except (pymysql.Error, pymysql.Warning) as e:
                print("Breaking Error: Variants per Screening query exception: " + str(e), file=sys.stdout )
                f.write(str(greece_time) + "Breaking Error: Variants per Screening query exception: " + str(e) + "\n" + "Query executed: \n" + str(cursor._last_executed) )
                commit[file_num] = 0
                continue

            if ( cursor.rowcount != 0 ):
                print("variants_per_screening: ",cursor.rowcount, file=sys.stdout )
                f.write("variants_per_screening: %d \n" % int(cursor.rowcount) )
            else:
                print("variants_per_screening: ",0, file=sys.stdout )
                f.write("variants_per_screening: 0 \n")
                commit[file_num] = 0
                continue
        
        if commit[file_num] == 1:
            cursor.execute("ANALYZE TABLE lovd_variants")
            cursor.execute("ANALYZE TABLE lovd_individuals")
            cursor.execute("ANALYZE TABLE lovd_screenings")
            print("Commiting connection", file=sys.stdout)
            print("Variants import successfull", file=sys.stdout)
            f.write("Commiting connection \n")
            f.write("Variants import successfull \n")
            connection.commit()

        else:
            print("Connection rollback", file=sys.stdout)
            f.write("Connection rollback \n")

    cursor.close()
    connection.close()
    print("Closing connection", file=sys.stdout)
    f.write("Closing connection \n\n\n")
    f.close()

